<?php
/* @var $this yii\web\View */

$this->title = 'Sistem Cuti Karyawan';
?>

      <?php
    if (Yii::$app->session->hasFlash('note')):
        ?>
        <div class="alert alert-danger">
            <?php echo Yii::$app->session->getFlash('note'); ?>
        </div>
        <?php
    endif;
    ?>

<div class="site-warning">

      <div class="jumbotron">
      <h3>Terjadi Kesalahan, Request Anda tidak dapat dilakukan</h3>
    </div>
</div>
