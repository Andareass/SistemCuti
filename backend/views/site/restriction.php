<?php
/* @var $this yii\web\View */

$this->title = 'Sistem Cuti Karyawan';
?>

<?php
    if (Yii::$app->session->hasFlash('note')):
        ?>
        <div class="alert alert-danger">
            <?php echo Yii::$app->session->getFlash('note'); ?>
        </div>
        <?php
    endif;
    ?>

<div class="site-index">

    <div class="jumbotron">
      <h3>Anda tidak dapat melakukan aksi tersebut</h3>\
    </div>
</div>
