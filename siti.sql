-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2018 at 04:39 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siti`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_datacuti`
--

CREATE TABLE `t_datacuti` (
  `id_cuti` int(11) NOT NULL,
  `id_pcuti` int(11) NOT NULL,
  `tgl_sah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lama_sah` int(11) NOT NULL,
  `tgl_sah_mulai` date NOT NULL,
  `tgl_sah_akhir` date NOT NULL,
  `catatan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_datacuti`
--

INSERT INTO `t_datacuti` (`id_cuti`, `id_pcuti`, `tgl_sah`, `lama_sah`, `tgl_sah_mulai`, `tgl_sah_akhir`, `catatan`) VALUES
(1, 1, '2018-06-20 07:20:35', 1, '2018-07-27', '2018-07-28', NULL),
(2, 2, '2018-06-20 09:34:54', 5, '2018-07-13', '2018-07-18', NULL),
(3, 3, '2018-06-20 10:00:19', 2, '2018-07-11', '2018-07-13', NULL),
(4, 5, '2018-06-20 10:23:38', 2, '2018-07-11', '2018-07-13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_dataizin`
--

CREATE TABLE `t_dataizin` (
  `id_izin` int(11) NOT NULL,
  `id_pizin` int(11) NOT NULL,
  `tgl_sah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lama_sah` int(11) NOT NULL,
  `tgl_sah_mulai` date NOT NULL,
  `tgl_sah_akhir` date NOT NULL,
  `catatan` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `t_jabatan`
--

CREATE TABLE `t_jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_jabatan`
--

INSERT INTO `t_jabatan` (`id_jabatan`, `jabatan`) VALUES
(1, 'Rektor'),
(2, 'Wakil Rektor 1'),
(3, 'Wakil Rektor 2'),
(4, 'Wakil Rektor 3'),
(5, 'Dekan Fakultas Teknik Informatika dan Elektro'),
(6, 'Dekan Fakultas Teknologi Industri'),
(7, 'Dekan Fakultas Bio-Teknologi'),
(8, 'Kepala Prodi S1 Sistem Informasi'),
(9, 'Kepala Prodi S1 Teknik Informatika'),
(10, 'Kepala Prodi S1 Teknik Elektro'),
(11, 'Kepala Prodi D4 Teknik Informatika'),
(12, 'Kepala Prodi D3 Teknik Informatika'),
(13, 'Kepala Prodi D3 Teknik Komputer'),
(14, 'Kepala Prodi S1 Manajemen Rekayasa'),
(15, 'Kepala Prodi S1 Teknik Bioproses'),
(16, 'Dosen S1 SI'),
(17, 'Dosen S1 TI'),
(18, 'Dosen S1 TE'),
(19, 'Dosen D4 TI'),
(20, 'Dosen D3 TI'),
(21, 'Dosen D3 TK'),
(22, 'Dosen S1 MR'),
(23, 'Dosen S1 TB'),
(24, 'Kepala Satpam'),
(25, 'Satpam'),
(26, 'Kepala Kantin'),
(27, 'Pegawai Kantin'),
(28, 'Koordinator Akademik dan Kemahasiswaan'),
(29, 'HRD');

-- --------------------------------------------------------

--
-- Table structure for table `t_jeniscuti`
--

CREATE TABLE `t_jeniscuti` (
  `id_jcuti` int(11) NOT NULL,
  `nama_cuti` varchar(30) NOT NULL,
  `lama_cuti` int(30) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_jeniscuti`
--

INSERT INTO `t_jeniscuti` (`id_jcuti`, `nama_cuti`, `lama_cuti`, `keterangan`) VALUES
(1, 'Cuti Tahunan', 18, 'Cuti tahunan yang diberikan untuk setiap pegawai (tergantung lama kerja)'),
(2, 'Cuti Nikah', 3, 'Cuti yang diberikan aturan cuti bagi pegawai yang ingin menikah'),
(3, 'Cuti Melahirkan', 90, 'Cuti yang diberikan bagi pegawai yang melahirkan'),
(4, 'Cuti Keguguran', 45, 'Cuti diberikan bagi pegawai yang mengalami keguguran'),
(5, 'Cuti Diluar Tanggungan', 180, 'Cuti ini diberikan bagi pegawai yang sedang memiliki permasalahan pribadi');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenisizin`
--

CREATE TABLE `t_jenisizin` (
  `id_jizin` int(11) NOT NULL,
  `nama_izin` varchar(50) NOT NULL,
  `lama_izin` int(11) NOT NULL,
  `keterangan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_jenisizin`
--

INSERT INTO `t_jenisizin` (`id_jizin`, `nama_izin`, `lama_izin`, `keterangan`) VALUES
(1, 'Izin Pernikahan', 3, 'Pegawai mengajukan izin untuk acara pernikahan'),
(2, 'Izin Keperluan Keluarga', 2, 'Pegawai menghadiri acara keluarga'),
(3, 'Izin Sakit', 2, 'Pegawai mengalami sakit ringan'),
(4, 'Izin Istri Melahirkan', 2, 'Pegawai mengajukan izin dikarenakan istri melahirkan');

-- --------------------------------------------------------

--
-- Table structure for table `t_karyawan`
--

CREATE TABLE `t_karyawan` (
  `id` int(11) NOT NULL,
  `nik` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(200) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `inisial` varchar(15) NOT NULL,
  `id_sex` enum('Male','Female') NOT NULL,
  `status_kawin` enum('Kawin','Belum Kawin') NOT NULL,
  `d_aw_kerja` date NOT NULL,
  `status_kepeg` enum('Tetap','Kontrak') NOT NULL,
  `id_jabatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_karyawan`
--

INSERT INTO `t_karyawan` (`id`, `nik`, `nama`, `email`, `no_hp`, `inisial`, `id_sex`, `status_kawin`, `d_aw_kerja`, `status_kepeg`, `id_jabatan`) VALUES
(1, '11111110', 'Fidelis Haposan Silalahi', 'fidelis@del.ac.id', '082167839928', 'FHS', 'Male', 'Kawin', '2013-05-02', 'Tetap', 29),
(2, '11111111', 'Prof. Togar Simatupang', 'togar@gmail.com', '082369905465', 'TGS', 'Male', 'Kawin', '2010-06-07', 'Tetap', 1),
(3, '11111112', 'Arlinta Christy Barus', 'ibuharlinta@gmail.com', '081274728560', 'ACB', 'Female', 'Kawin', '2002-12-04', 'Tetap', 2),
(4, '11111113', 'Christoper Janwar Saputra Sinaga', 'christo@gmail.com', '085345678291', 'CHR', 'Male', 'Belum Kawin', '2017-05-01', 'Tetap', 3),
(5, '11111114', 'Dr. Ir. Bambang S.P. Abednego', 'bambang@gmail.com', '082267814572', 'BBG', 'Male', 'Kawin', '2016-01-09', 'Tetap', 4),
(6, '11111115', 'Tennov Simanjuntak, S.T., M.Sc.', 'tennov@del.ac.id', '082165145186', 'THS', 'Male', 'Kawin', '2001-08-13', 'Tetap', 5),
(7, '11111116', 'Dr. Yosef Barita Sar Manik, S.T., M.Sc.', 'yosef@del.ac.id', '081245627856', 'YMA', 'Male', 'Kawin', '2014-10-27', 'Tetap', 6),
(8, '11111117', 'Dr. Merry Meryam Martgrita, S.Si., M.Si', 'merry@del.ac.id', '081245681234', 'MMM', 'Female', 'Belum Kawin', '2014-01-20', 'Tetap', 7),
(9, '11111118', 'Mario Elyezer Subekti Simaremare', 'mario@gmail.com', '082245543221', 'MSS', 'Male', 'Kawin', '2018-06-01', 'Tetap', 8),
(10, '11111119', 'Inte Christinawati Bu\'ulolo, S.T., M.T.I.', 'inte@del.ac.id', '081245678910', 'ICB', 'Female', 'Kawin', '2007-03-19', 'Tetap', 9),
(11, '11111120', 'Indra Hartarto Tambunan, Ph.D.', 'indra@del.ac.id', '081312345678', 'IHT', 'Male', 'Kawin', '2016-06-06', 'Tetap', 10),
(12, '11111121', 'Roy Deddy Lumban Tobing, S.T., M.ICT.', 'roy@del.ac.id', '085212345678', 'RDT', 'Male', 'Kawin', '2017-08-28', 'Tetap', 11),
(13, '11111122', 'Anthon Roberto Tampubolon, S.Kom., M.T.', 'anton@del.ac.id', '085245678923', 'ART', 'Male', 'Kawin', '2015-07-02', 'Tetap', 12),
(14, '11111123', 'Pandapotan Siagian, S.T., M.M.', 'pandapotan.siagian@del.ac.id', '081345684589', 'PDS', 'Male', 'Kawin', '0000-00-00', 'Tetap', 13),
(15, '11111124', 'Devin Wawan Saputra, S.T., M.B.A.', 'devis@del.ac.id', '085214567894', 'DWS', 'Male', 'Kawin', '2016-05-23', 'Tetap', 14),
(16, '11111125', 'Adellina Manurung, S.Si., M.Sc.', 'adelina@del.ac.id', '082315489856', 'ANM', 'Female', 'Kawin', '2016-01-15', 'Tetap', 15),
(17, '11111126', 'Samuel Indra Gunawan Situmeang', 'samuel@del.ac.id', '082170709369', 'SAM', 'Male', 'Belum Kawin', '2017-08-16', 'Tetap', 16),
(18, '11111127', 'Lit Malem Ginting, S.Si., M.T', 'litmalem@del.ac.id', '082312345678', 'LMG', 'Male', 'Kawin', '2016-01-06', 'Tetap', 17),
(19, '11111128', 'Christin Erniati Panjaitan, S.T., M.Sc', 'christin@del.ac.id', '081245689456', 'CEP', 'Female', 'Belum Kawin', '2016-03-15', 'Tetap', 18),
(20, '11111129', 'Riyanthi Aggrainy Sianturi, S.Sos., M.Ds', 'riyanthi@del.ac.id', '081345678953', 'RIS', 'Female', 'Belum Kawin', '2011-03-01', 'Tetap', 19),
(21, '11111130', 'Teamsar Muliadi Panggabean', 'teamsar@del.ac.id', '081245678956', 'TMP', 'Male', 'Belum Kawin', '2017-08-28', 'Tetap', 20),
(22, '11111131', 'Gerry Italiano Wowiling, S.Tr.Kom', 'gerry@del.ac.id', '085245679658', 'GIW', 'Male', 'Belum Kawin', '2015-08-25', 'Tetap', 21),
(23, '11111132', 'Niko Saripson Simamora', 'niko@del.ac.id', '085245789658', 'NSS', 'Male', 'Belum Kawin', '2018-01-15', 'Tetap', 22),
(24, '11111133', 'Andy Trirakhmadi, S.T., M.T', 'andy@del.ac.id', '081345698745', 'ARR', 'Male', 'Belum Kawin', '2015-10-19', 'Tetap', 23),
(25, '11111134', 'A Aruan', 'aruan@del.ac.id', '085352648956', 'ARN', 'Male', 'Kawin', '2016-05-23', 'Tetap', 24),
(26, '11111135', 'Ariston Pasaribu', 'aristone@del.ac.id', '082345789658', 'ARS', 'Male', 'Kawin', '0000-00-00', 'Tetap', 25),
(37, '11111136', 'Verawaty Situmorang', 'verawaty@del.ac.id', '082356784321', 'VES', 'Female', 'Kawin', '2012-03-02', 'Tetap', 19),
(38, '11111137', 'Nenni Aruan', 'nenni@del.ac.id', '085234568907', 'NNA', 'Female', 'Belum Kawin', '2016-09-01', 'Tetap', 17),
(39, '11111138', 'Yuniarta Basani', 'yuni@del.ac.id', '085764325678', 'YUN', 'Female', 'Belum Kawin', '2015-09-07', 'Tetap', 19),
(40, '11111139', 'Tulus Simanjuntak', 'tulus@del.ac.id', '087985746653', 'TLS', 'Male', 'Belum Kawin', '2016-09-06', 'Tetap', 19);

-- --------------------------------------------------------

--
-- Table structure for table `t_mastercuti_izin`
--

CREATE TABLE `t_mastercuti_izin` (
  `id` int(11) NOT NULL,
  `kuota_cuti` int(11) NOT NULL,
  `kuota_cuti_n` int(11) NOT NULL,
  `kuota_cuti_m` int(11) NOT NULL,
  `kuota_cuti_k` int(11) NOT NULL,
  `kuota_cuti_d` int(11) NOT NULL,
  `jlh_izin` int(11) NOT NULL,
  `lama_kerja` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_mastercuti_izin`
--

INSERT INTO `t_mastercuti_izin` (`id`, `kuota_cuti`, `kuota_cuti_n`, `kuota_cuti_m`, `kuota_cuti_k`, `kuota_cuti_d`, `jlh_izin`, `lama_kerja`, `id_karyawan`) VALUES
(2, 15, 0, 0, 0, 100, 3, 0, 5),
(3, 18, 0, 100, 100, 100, 0, 0, 3),
(5, 12, 3, 0, 0, 12, 1, 0, 4),
(6, 18, 0, 0, 0, 100, 15, 0, 1),
(7, 18, 0, 0, 0, 180, 9, 0, 6),
(8, 15, 0, 0, 0, 180, 9, 0, 7),
(9, 15, 3, 90, 45, 180, 10, 0, 8),
(10, 18, 3, 90, 45, 180, 10, 0, 10),
(11, 12, 3, 0, 0, 180, 9, 0, 11),
(12, 18, 3, 0, 0, 180, 9, 0, 12),
(13, 15, 3, 0, 0, 180, 9, 0, 13),
(14, 15, 3, 0, 0, 180, 9, 0, 14),
(15, 12, 3, 0, 0, 180, 9, 0, 15),
(16, 15, 3, 90, 45, 180, 10, 0, 16),
(17, 12, 1, 0, 0, 99, 7, 0, 17),
(18, 15, 3, 0, 0, 180, 9, 0, 18),
(19, 12, 3, 90, 45, 180, 10, 0, 19),
(20, 18, 3, 90, 45, 180, 10, 0, 20),
(21, 0, 3, 0, 0, 0, 9, 0, 21),
(22, 15, 3, 0, 0, 180, 9, 0, 22),
(23, 0, 3, 0, 0, 0, 9, 0, 23),
(24, 15, 3, 0, 0, 180, 9, 0, 24),
(25, 12, 3, 0, 0, 180, 9, 0, 25),
(26, 14, 3, 0, 0, 180, 9, 0, 26),
(31, 12, 0, 0, 0, 100, 12, 0, 9),
(32, 0, 0, 0, 0, 0, 0, 0, 2),
(34, 13, 3, 90, 45, 180, 10, 0, 37),
(35, 12, 1, 90, 45, 180, 10, 0, 38),
(36, 15, 3, 90, 45, 180, 10, 0, 39),
(37, 12, 3, 0, 0, 180, 9, 0, 40);

-- --------------------------------------------------------

--
-- Table structure for table `t_permohonancuti`
--

CREATE TABLE `t_permohonancuti` (
  `id_pcuti` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `id_jcuti` int(11) NOT NULL,
  `tgl_pengajuan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tgl_mulai_cuti` date NOT NULL,
  `tgl_akhir_cuti` date NOT NULL,
  `lama_cuti` int(11) NOT NULL,
  `pengalihan` varchar(250) DEFAULT NULL,
  `alasan_cuti` varchar(250) NOT NULL,
  `id_atasan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_permohonancuti`
--

INSERT INTO `t_permohonancuti` (`id_pcuti`, `id`, `id_jcuti`, `tgl_pengajuan`, `tgl_mulai_cuti`, `tgl_akhir_cuti`, `lama_cuti`, `pengalihan`, `alasan_cuti`, `id_atasan`) VALUES
(1, 17, 5, '2018-06-20 07:19:32', '2018-07-27', '2018-07-28', 1, 'asds', 'sda', 8),
(2, 37, 1, '2018-06-20 09:32:45', '2018-07-13', '2018-07-18', 5, 'Dialihkan kepada ibu Rini', 'Acara Keluarga', 11),
(3, 38, 2, '2018-06-20 09:59:04', '2018-07-11', '2018-07-13', 2, 'dialihkan kepada ibu Inte', 'Acara pernikahan', 9),
(4, 38, 1, '2018-06-20 10:04:47', '2018-07-11', '2018-07-17', 6, 'dialihkan kepada ibu Inte', 'Acara pernikahan keluarga', 9),
(5, 17, 2, '2018-06-20 10:22:09', '2018-07-11', '2018-07-13', 2, 'dialihkan kepada ibu Parmonangan', 'Acara pernikahan', 8);

-- --------------------------------------------------------

--
-- Table structure for table `t_permohonanizin`
--

CREATE TABLE `t_permohonanizin` (
  `id_pizin` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `id_jizin` int(11) NOT NULL,
  `tgl_pengajuan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lama_izin` int(11) NOT NULL,
  `pengalihan` varchar(250) DEFAULT NULL,
  `id_atasan` int(11) NOT NULL,
  `alasan_izin` varchar(250) DEFAULT NULL,
  `tgl_mulai_izin` date NOT NULL,
  `tgl_akhir_izin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_permohonanizin`
--

INSERT INTO `t_permohonanizin` (`id_pizin`, `id`, `id_jizin`, `tgl_pengajuan`, `lama_izin`, `pengalihan`, `id_atasan`, `alasan_izin`, `tgl_mulai_izin`, `tgl_akhir_izin`) VALUES
(1, 37, 2, '2018-06-20 09:38:21', 1, 'dialihkan kepada ibu Rini', 11, 'acara keluarga', '2018-07-12', '2018-07-13'),
(2, 38, 2, '2018-06-20 10:02:25', 2, 'Dialihkan kepada pak Yahya', 9, 'Acara keluarga', '2018-07-16', '2018-07-18'),
(3, 38, 2, '2018-06-20 10:02:26', 2, 'Dialihkan kepada pak Yahya', 9, 'Acara keluarga', '2018-07-16', '2018-07-18');

-- --------------------------------------------------------

--
-- Table structure for table `t_struktur`
--

CREATE TABLE `t_struktur` (
  `id_struktur` int(11) NOT NULL,
  `id_atasan` int(11) NOT NULL COMMENT 'id dari tabel jabatan',
  `id_bawahan` int(11) NOT NULL COMMENT 'id dari tabel jabatan yang menjadi bawahan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_struktur`
--

INSERT INTO `t_struktur` (`id_struktur`, `id_atasan`, `id_bawahan`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 1, 4),
(4, 3, 5),
(5, 3, 6),
(6, 3, 7),
(7, 5, 8),
(8, 5, 9),
(9, 5, 10),
(10, 5, 11),
(12, 5, 12),
(13, 5, 13),
(14, 6, 14),
(15, 8, 16),
(16, 9, 17),
(17, 10, 18),
(18, 11, 19),
(19, 12, 20),
(20, 13, 21),
(21, 14, 22),
(22, 15, 23),
(23, 3, 24),
(24, 24, 25),
(25, 3, 26),
(26, 26, 27),
(27, 26, 27),
(28, 3, 28),
(29, 3, 28),
(30, 3, 29),
(31, 3, 29);

-- --------------------------------------------------------

--
-- Table structure for table `t_sts_permohonancuti`
--

CREATE TABLE `t_sts_permohonancuti` (
  `id_status` int(11) NOT NULL,
  `id_pcuti` int(11) NOT NULL,
  `jlh_permohonan` int(11) NOT NULL,
  `id_atasan` int(11) NOT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `alasan_reject` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_sts_permohonancuti`
--

INSERT INTO `t_sts_permohonancuti` (`id_status`, `id_pcuti`, `jlh_permohonan`, `id_atasan`, `keterangan`, `status`, `alasan_reject`) VALUES
(1, 1, 1, 8, 'sda', 'Confirm Wakil Rektor 2', NULL),
(2, 2, 1, 11, 'Acara Keluarga', 'Confirm Wakil Rektor 2', NULL),
(3, 3, 1, 9, 'Acara pernikahan', 'Confirm Wakil Rektor 2', NULL),
(4, 4, 1, 9, 'Acara pernikahan keluarga', 'Confirm Atasan Langsung', NULL),
(5, 5, 1, 8, 'Acara pernikahan', 'Confirm Wakil Rektor 2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_sts_permohonanizin`
--

CREATE TABLE `t_sts_permohonanizin` (
  `id_status` int(11) NOT NULL,
  `id_pizin` int(11) NOT NULL,
  `jlh_permohonan` int(11) DEFAULT NULL,
  `id_atasan` int(11) NOT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `status` varchar(200) NOT NULL,
  `alasan_reject` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_sts_permohonanizin`
--

INSERT INTO `t_sts_permohonanizin` (`id_status`, `id_pizin`, `jlh_permohonan`, `id_atasan`, `keterangan`, `status`, `alasan_reject`) VALUES
(1, 1, 1, 11, 'acara keluarga', 'Belum Dikonfirmasi', NULL),
(2, 2, 1, 9, 'Acara keluarga', 'Belum Dikonfirmasi', NULL),
(3, 3, 1, 9, 'Acara keluarga', 'Belum Dikonfirmasi', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nik` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nik`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`) VALUES
(1, '11111110', 'pakfidelis', '6wjxKXC0ndN6dSDPk_C7Z-KKK-hpNd13', '$2y$13$Wq.4aq.paC2rDmsfDQEdueRFicROH6HxnwC3xbCvIyH5GNUIxMExW', NULL, 'fidelis@del.ac.id', 10),
(2, '11111111', 'paktogar', 'FexmJUaL8HcZvIwYwuxctRHQ_b6cBywO', '$2y$13$W59QKQWNnwi5uFqsfmu5ReHo5SI1GFM5TvAw.kUCzQY1s5GCjjzjm', NULL, 'togar@gmail.com', 10),
(3, '11111112', 'ibuharlinta', 'punJGgf499rHvvzU0nCipG59jTAgAnx2', '$2y$13$iTv2jfYm4ZgTXbfyPRJNEuAj5Agy0Iv7pU7PANjUD88FlVQj1IJHC', NULL, 'andareassianipar@gmail.com', 10),
(4, '11111113', 'pakchristo', '_ZKk8hH8v0enuTZvI9kE-g7RFdqJEInl', '$2y$13$8MyIFy/UAQa7Bbo6yOEwqeJyQy9uqfURcG5vVxMU0GbCQQNZo2rO6', NULL, 'christo@gmail.com', 10),
(5, '11111114', 'pakbambang', 'RGTMX5dh7EbsAiOywz_L5lAkCd4rz6gi', '$2y$13$IM3sjqT3fmPG6a1fCbm6juD13g4gNJZRLjItf9RbMpCXb1u2sE.qa', NULL, 'andareassianipar@gmail.com', 10),
(6, '11111115', 'paktennov', '85uFuJtj8vv-hbsapOSJeP0WMvxdBuxl', '$2y$13$HWUQO1Mr2hyUZMsdvxwpuuSRIHbFyMW9Ivu7u45TPm4.BTThqJrZy', NULL, 'tennov@del.ac.id', 10),
(7, '11111116', 'pakyosef', 'R8EadFGeZ1pGGxWope08oTfjWwh8A9PQ', '$2y$13$EzWngR3rMnLqCvr8yS5msO/EjTdVu5p5A0qDUGfmt0khq9v3SxjIe', NULL, 'yosef@del.ac.id', 10),
(8, '11111117', 'ibumerry', 'xDc7Br97Pm8_sm7RhBf3bqtmTdnnQyht', '$2y$13$lCjlBwBCKYvrURJkTtiB2.aAuXyI85Ptj8MaPNHwbyNkYTaIz/lvK', NULL, 'merry@del.ac.id', 10),
(9, '11111118', 'pakmario', 'ZTdkaxcaPLd5CUBE7qEIqk6t1np18MRT', '$2y$13$.rFtq54/A3yzQ101PCTVsu6oOwxGFIPRV66YfS5CgzOYu1Faway0e', NULL, 'mario@gmail.com', 10),
(10, '11111119', 'ibuinte', 'Lprk336GAujqV0nHQN1aYXxbGXf8FAXS', '$2y$13$qkHDgmP5uHOeN6sHncvK.u/ugqFz5AavOsYaKG/oQbvvOB1Hr8Aoe', NULL, 'inte@del.ac.id', 10),
(11, '11111120', 'pakindra', 'CI0IivqYC8rN27CDDtXmp9T1debOw5RI', '$2y$13$UZGMQDVbGA3qW9j6VUUpD.YFvLs7GWmeDk.vTHoek6OSmL8DNM49W', NULL, 'indra@del.ac.id', 10),
(12, '11111121', 'pakroy', 's3YGlM__RphtXHN-UrkXwiSHehR13ntu', '$2y$13$kMfm/Ur7OUHLtR0yF7gnJuY2KqnRCVOOrpT7Kf9wi8utiF1J5Cfla', NULL, 'roy@del.ac.id', 10),
(13, '11111122', 'pakanton', 'lLiRqFb1zgNa9RJomh7voHyVct1orlvl', '$2y$13$SJIbgIaGPG774qtf3/PGL.yU617od33THzEXlvaSv40SUTX21rMtC', NULL, 'anton@del.ac.id', 10),
(14, '11111123', 'pakpandapotan', 'bCWiw0wf-Ni7iImWiwMMK9Mk-w_w3mNw', '$2y$13$RvbdMpOzL.sQVSQ.F87XqexvZDNMPyVPxny4rsZI5asMnyfEP6ifO', NULL, 'pandapotan.siagian@del.ac.id', 10),
(15, '11111124', 'pakdevis', 'KzTNoyfzQRxTJ4cJsEokVpETGLrDFq70', '$2y$13$qcP94OzSWx2X0RXi6Py6J.ycwiQ7Pkp4Hi70xE0Wnr3wo8.hy2LxC', NULL, 'devis@del.ac.id', 10),
(16, '11111125', 'ibuadelina', 'YlCAs2zFWvteI5d1EvGcFriU2TS8jwLa', '$2y$13$aZ3xSwAbmmHwzcF7A8gopuMGSkZA8D8.q9QWUcai7ZzV65o2W7QAi', NULL, 'adelina@del.ac.id', 10),
(17, '11111126', 'paksamuel', 'jT24sALiKqiX6XjpywE1ugr2hQDOBNK5', '$2y$13$KTizQlhOZyno/04YSaUMxu8gQALhVi7pFcdmmy8Is.zFco46ocEQ2', NULL, 'samuel@del.ac.id', 10),
(18, '11111127', 'paklitmalem', 'zPk4DFGvBZiPKGC895kTmvIz5k7NWC54', '$2y$13$kko/BYDAC97K4yspTazmQOs82RPsMFkhe6Pq6RawQL/vRi.OohogK', NULL, 'litmalem@del.ac.id', 10),
(19, '11111128', 'ibuchristin', 'Aisf-msQrWWBgO7U0duRttSAizKtHzdH', '$2y$13$8223er2hYGFtOdEKEE8iNeGFOSufjPauDjM/jalQa5QyUjjrXL6EC', NULL, 'christin@del.ac.id', 10),
(20, '11111129', 'iburiyanthi', '7H9vrci8IlmQdxN9Xnxk2eAVS5yE-DuZ', '$2y$13$Y/5vIwN3mWX/6Au8EZD8nOPLl/EOj/fyAEFbHbFFYFy61ZXMBr9OG', NULL, 'riyanthi@del.ac.id', 10),
(21, '11111130', 'pakteamsar', 'Eab1Jh8GwDYRkWhCwMbq6xerljweLvx6', '$2y$13$crGrZglWi2nmiunGFvcCO.3yuFkWSss4JJB5YdXDsy.5K.hMwxDl6', NULL, 'teamsar@del.ac.id', 10),
(22, '11111131', 'pakgerry', 'o2Dw40nT09xC3ONHR0cIBlbPdEy0-4P7', '$2y$13$HaKTPTdYwvsFvqjuHiQwue5MUxJzl13Pvlc7qAIC2SPeHj8lnD2HW', NULL, 'gerry@del.ac.id', 10),
(23, '11111132', 'pakniko', 'IfddXfvgX1EMi-ao1vshpzeYHVWolhvi', '$2y$13$ZfOD0/EMIQqney1bKnwpYO/xZhHSh6clTMFIxJxkZ0H91dYTjBD6i', NULL, 'niko@del.ac.id', 10),
(24, '11111133', 'pakandy', '9x_h3RsLyzBfRrXJwVlFw8MF1BDA_-ZL', '$2y$13$gAF49XiDq9QmYfV3GkKWYu/RnnL1xGXSu90KGG92fj7UqjIT9EqeS', NULL, 'andy@del.ac.id', 10),
(25, '11111134', 'pakaruan', '8ivyOeNZMrjee-Yfa3FwWcdmW_gmn1Rh', '$2y$13$S5/UJNGoR7nrMUI/N2ws.uAcYBzHWJVFJBtvy4AbTrC9/x5/uVg3i', NULL, 'aruan@del.ac.id', 10),
(26, '11111135', 'pakariston', 'o3ky-GR_9jUVg91oUbLF0T0ewNMWULdF', '$2y$13$/WEIHqSf.G92Bs/t8nTD1eMlegzC.Qj7mWfp15Aw5LNSqwzPsLk0S', NULL, 'aristone@del.ac.id', 10),
(27, '11111154', 'asd', 'P2BM87dHrcHMqDIuGrWPunDQbPkH_KDR', '$2y$13$3zxCM8gEDLSGmbt4Cn8OO.YPc1PsECZVKoR8A9YWDXiyRb0FCTbOy', NULL, 'admin@gmail.comm', 10),
(28, '1312312', 'adsdgfd', 'AuAPDpfakDtKe_dyZr25ZYUpO2A_RV9-', '$2y$13$ep7QKkTfhU0U63d2UngZCuRhpPRV3wtZsSYjHJPdjPx8JsMg8hZbG', NULL, 'andareassianipar@gmail.com', 10),
(29, '312312', 'sadasd', '66FDAJP1pj-g2-gbMMaenosHXHZUhkmo', '$2y$13$1ANco9lQXl6VNZFjvbfdBeq/Eb7hRL0aVkb5LceQAG/.bNCcDx8c6', NULL, 'andersareda@gmail.com', 10),
(33, '3245678', 'sad', 'fcx0wZFOmOoPd-R_N_u7lkPlYhY2niMQ', '$2y$13$/wU0JMAtEcYDxsWGQFO6m./ayn043.lzKJ5fssL0mKKhztelL69R.', NULL, 'andareassianipar@gmail.com', 10),
(34, '123456', 'adasdas', 'Bc1q0S3OVjLj6mBItepJEz1LADgLKVl0', '$2y$13$47V.88lLOp7Oc6sdVsBBEez0G1EJmIPp5V97Jq.Us2/kMxAHA48x6', NULL, 'admin@gmail.com', 10),
(35, '11111177', 'addsd', 'dnVDm117QxPTyMzu3Ri-sho-GnG-TmS7', '$2y$13$r6lrR4uDiBdd4jtCOYcdSe28VWdY61E8TXNMg5rHZgXcRDws6DGfK', NULL, 'admin@gmail.com', 10),
(36, '123459', 'lll', 'JkJ3N-avIoGH-75-TDieYE5oJl3X3bRk', '$2y$13$1yZcMLPBIgbMF3SPxStDxuVqfHwgaTb7SWIMow3yUnLwLyKTEO1wS', NULL, 'andareassianipar@gmail.com', 10),
(37, '11111136', 'ibuvera', 'vDmg65otPH2dVLCpVxA3-lRnfEcDtb9X', '$2y$13$xt0Uj/SFLsQeriHLD0V00.GM8dS4rfjkAE.nbJqmiLPu3x33uzTH6', NULL, 'verawaty@del.ac.id', 10),
(38, '11111137', 'ibunenni', 'LgRigVay7Wzi83c1PNm74pM9Ywn9zKH2', '$2y$13$UwZFxjP81CPEvG55Fm6.lOTEg9ilGZ2o33R46VJod.jFmb5vOOUQS', NULL, 'nenni@del.ac.id', 10),
(39, '11111138', 'ibuyuni', 'x0p8qT0OTb3HbgN9WOhxrNfI0HCNpwk9', '$2y$13$SaV85xoUEKKHJHS8FBZ9keJPAp1.pKWtYRhDrfeiOgfgYfwBz6TDm', NULL, 'yuni@del.ac.id', 10),
(40, '11111139', 'paktulus', 'kBmtGC04SOON0HSzseZljUtLwg8mvwPF', '$2y$13$oeutmla1PuN0rnnL4xdJsuFpaOVnSyi68KH48fpZJdcJd75zwaDoW', NULL, 'tulus@del.ac.id', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_datacuti`
--
ALTER TABLE `t_datacuti`
  ADD PRIMARY KEY (`id_cuti`),
  ADD KEY `id_pcuti` (`id_pcuti`);

--
-- Indexes for table `t_dataizin`
--
ALTER TABLE `t_dataizin`
  ADD PRIMARY KEY (`id_izin`),
  ADD KEY `fk_cut_r_data_izin_cut_d_permohonan_izin1` (`id_pizin`);

--
-- Indexes for table `t_jabatan`
--
ALTER TABLE `t_jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `t_jeniscuti`
--
ALTER TABLE `t_jeniscuti`
  ADD PRIMARY KEY (`id_jcuti`);

--
-- Indexes for table `t_jenisizin`
--
ALTER TABLE `t_jenisizin`
  ADD PRIMARY KEY (`id_jizin`);

--
-- Indexes for table `t_karyawan`
--
ALTER TABLE `t_karyawan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nip` (`nik`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indexes for table `t_mastercuti_izin`
--
ALTER TABLE `t_mastercuti_izin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pegawai` (`id_karyawan`);

--
-- Indexes for table `t_permohonancuti`
--
ALTER TABLE `t_permohonancuti`
  ADD PRIMARY KEY (`id_pcuti`),
  ADD KEY `id` (`id`),
  ADD KEY `id_jcuti` (`id_jcuti`),
  ADD KEY `id_atasan` (`id_atasan`);

--
-- Indexes for table `t_permohonanizin`
--
ALTER TABLE `t_permohonanizin`
  ADD PRIMARY KEY (`id_pizin`),
  ADD KEY `id_jizin_pegawai` (`id_jizin`),
  ADD KEY `id_peg` (`id`),
  ADD KEY `id_atasan2` (`id_atasan`);

--
-- Indexes for table `t_struktur`
--
ALTER TABLE `t_struktur`
  ADD PRIMARY KEY (`id_struktur`),
  ADD KEY `id_atasan` (`id_atasan`),
  ADD KEY `id_bawahan` (`id_bawahan`);

--
-- Indexes for table `t_sts_permohonancuti`
--
ALTER TABLE `t_sts_permohonancuti`
  ADD PRIMARY KEY (`id_status`),
  ADD KEY `id_pcutiIzin` (`id_pcuti`),
  ADD KEY `id_atasanlangsung` (`id_atasan`);

--
-- Indexes for table `t_sts_permohonanizin`
--
ALTER TABLE `t_sts_permohonanizin`
  ADD PRIMARY KEY (`id_status`),
  ADD KEY `id_pizin` (`id_pizin`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nip` (`nik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_datacuti`
--
ALTER TABLE `t_datacuti`
  MODIFY `id_cuti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_dataizin`
--
ALTER TABLE `t_dataizin`
  MODIFY `id_izin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_jabatan`
--
ALTER TABLE `t_jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `t_jeniscuti`
--
ALTER TABLE `t_jeniscuti`
  MODIFY `id_jcuti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_jenisizin`
--
ALTER TABLE `t_jenisizin`
  MODIFY `id_jizin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_karyawan`
--
ALTER TABLE `t_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `t_mastercuti_izin`
--
ALTER TABLE `t_mastercuti_izin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `t_permohonancuti`
--
ALTER TABLE `t_permohonancuti`
  MODIFY `id_pcuti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_permohonanizin`
--
ALTER TABLE `t_permohonanizin`
  MODIFY `id_pizin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_struktur`
--
ALTER TABLE `t_struktur`
  MODIFY `id_struktur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `t_sts_permohonancuti`
--
ALTER TABLE `t_sts_permohonancuti`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_sts_permohonanizin`
--
ALTER TABLE `t_sts_permohonanizin`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_datacuti`
--
ALTER TABLE `t_datacuti`
  ADD CONSTRAINT `id_pcuti` FOREIGN KEY (`id_pcuti`) REFERENCES `t_permohonancuti` (`id_pcuti`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_dataizin`
--
ALTER TABLE `t_dataizin`
  ADD CONSTRAINT `fk_cut_r_data_izin_cut_d_permohonan_izin1` FOREIGN KEY (`id_pizin`) REFERENCES `t_permohonanizin` (`id_pizin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `t_karyawan`
--
ALTER TABLE `t_karyawan`
  ADD CONSTRAINT `id_jabatan` FOREIGN KEY (`id_jabatan`) REFERENCES `t_jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_karyawan_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `user` (`nik`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_mastercuti_izin`
--
ALTER TABLE `t_mastercuti_izin`
  ADD CONSTRAINT `id` FOREIGN KEY (`id_karyawan`) REFERENCES `t_karyawan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_permohonancuti`
--
ALTER TABLE `t_permohonancuti`
  ADD CONSTRAINT `id_jcuti_pegawai` FOREIGN KEY (`id_jcuti`) REFERENCES `t_jeniscuti` (`id_jcuti`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_karyawan` FOREIGN KEY (`id`) REFERENCES `t_karyawan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_permohonancuti_ibfk_1` FOREIGN KEY (`id_atasan`) REFERENCES `t_jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_permohonanizin`
--
ALTER TABLE `t_permohonanizin`
  ADD CONSTRAINT `id_atasan2` FOREIGN KEY (`id_atasan`) REFERENCES `t_jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_jizin_pegawai` FOREIGN KEY (`id_jizin`) REFERENCES `t_jenisizin` (`id_jizin`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_peg` FOREIGN KEY (`id`) REFERENCES `t_karyawan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_struktur`
--
ALTER TABLE `t_struktur`
  ADD CONSTRAINT `id_atasan` FOREIGN KEY (`id_atasan`) REFERENCES `t_jabatan` (`id_jabatan`),
  ADD CONSTRAINT `id_bawahan` FOREIGN KEY (`id_bawahan`) REFERENCES `t_jabatan` (`id_jabatan`);

--
-- Constraints for table `t_sts_permohonancuti`
--
ALTER TABLE `t_sts_permohonancuti`
  ADD CONSTRAINT `id_atasanlangsung` FOREIGN KEY (`id_atasan`) REFERENCES `t_jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_pcutiIzin` FOREIGN KEY (`id_pcuti`) REFERENCES `t_permohonancuti` (`id_pcuti`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_sts_permohonanizin`
--
ALTER TABLE `t_sts_permohonanizin`
  ADD CONSTRAINT `id_pizin` FOREIGN KEY (`id_pizin`) REFERENCES `t_permohonanizin` (`id_pizin`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
