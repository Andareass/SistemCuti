Pengguna diharapkan menggunakan XAMPP Version: 5.6.36 agar fungsi dapat dijalankan secara keseluruhan.

- Aktifkan XAMPP ( Apache Server, SQL Database )

- Copy seluruh file direktori project ke folder C:\xampp\htdocs (example)

- Import database siti.sql kedalam database system

- Buka project melalui browser sesuai path direktori project
http://localhost/siti/backend/web

- Login dengan username/password : username/'jabatan'123
User example :
- pakfidelis/hrd123
- pakchristo/wr2123
- paktennov/dekanftie123
- pakmario/kaprodisi123
- paksamuel/dosensi123